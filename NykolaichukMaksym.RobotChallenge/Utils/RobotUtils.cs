﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NykolaichukMaksym.RobotChallenge.Utils
{
    public static class RobotUtils
    {

        public static Position OneStepTowardsPosition(Position @from, Position to)
        {
            var yChange = @from.Y > to.Y ? -1 : (@from.Y == to.Y ? 0 : 1);
            var xChange = @from.X > to.X ? -1 : (@from.X == to.X ? 0 : 1);

            return new Position()
            {
                X = @from.X + xChange,
                Y = @from.Y + yChange
            };
        }
        
        public static Position CertainStepsTowardsPosition(Position @from, Position to, int stepsCount)
        {
            var tempPosition = new Position(@from.X, @from.Y);
            for (int i = 0; i < stepsCount; i++)
            {
                tempPosition = OneStepTowardsPosition(tempPosition, to);
            }

            return tempPosition;
        }

        public static int MyRobotsAroundPosition(string ownerName, Position position, IList<Robot.Common.Robot> robots)
        {
            return robots
                .Where(x => x.OwnerName == ownerName)
                .Where(x => x.Position.X - position.X <= Constants.MINDISTANCETOCOLLECTENERGY)
                .Where(x => position.X - x.Position.X <= Constants.MINDISTANCETOCOLLECTENERGY)
                .Where(y => position.Y - y.Position.Y <= Constants.MINDISTANCETOCOLLECTENERGY)
                .Count(y => y.Position.Y - position.Y <= Constants.MINDISTANCETOCOLLECTENERGY);
        }

        public static bool CanMoveInOneStep(Robot.Common.Robot robot, Position position)
        {
            return CalculateEnergyOnRoad(robot.Position, position) <= robot.Energy;
        }

        public static EnergyStation FindNearestStation(Robot.Common.Robot robot, IList<EnergyStation> energyStations)
        {
            var nearestEnergyStation = energyStations[0];
            foreach (var station in energyStations)
            {
                if (CalculateDistance(station.Position, robot.Position) < CalculateDistance(nearestEnergyStation.Position, robot.Position))
                {
                    nearestEnergyStation = station;
                }
            }
            return nearestEnergyStation;
        }

        public static EnergyStation FindNearestNotTakenStation(string ownerName, Robot.Common.Robot robot, IList<Robot.Common.Robot> allRobots,
            IList<EnergyStation> energyStations)
        {
            var energyStationsCopy = new List<EnergyStation>(energyStations);
            energyStationsCopy.RemoveAll(x => MyRobotsAroundPosition(ownerName, x.Position, allRobots) >= Constants.LOCKEDSTATIONRULE);
            return FindNearestStation(robot, energyStationsCopy);
        }

        public static double CalculateEnergyOnRoad(Position first, Position second)
        {
            return Math.Pow(CalculateDistance(first, second), 2);
        }

        public static double CalculateDistance(Position first, Position second)
        {
            return Math.Sqrt(Math.Pow((first.X - second.X), 2) + Math.Pow((first.Y - second.Y), 2));
        }

        public static IList<EnergyStation> SortByDistance(Robot.Common.Robot robot, IList<EnergyStation> energyStations)
        {
            var tempEnergyStation = energyStations[0];
            var tempList = new List<EnergyStation>(energyStations);

            for (int j = 0; j <= tempList.Count - 2; j++)
            {
                for (int i = 0; i <= tempList.Count - 2; i++)
                {
                    if (CalculateDistance(robot.Position, tempList[i].Position) > CalculateDistance(robot.Position, tempList[i + 1].Position))
                    {
                        tempEnergyStation = tempList[i + 1];
                        tempList[i + 1] = tempList[i];
                        tempList[i] = tempEnergyStation;
                    }
                }
            }
            return tempList;
        }

        public static IList<EnergyStation> SortByEnergy(IList<EnergyStation> energyStations)
        {
            var tempList = new List<EnergyStation>(energyStations);
            var tempEnergyStation = energyStations[0];

            for (int j = 0; j <= tempList.Count - 2; j++)
            {
                for (int i = 0; i <= tempList.Count - 2; i++)
                {
                    if (tempList[i].Energy > tempList[i + 1].Energy)
                    {
                        tempEnergyStation = tempList[i + 1];
                        tempList[i + 1] = tempList[i];
                        tempList[i] = tempEnergyStation;
                    }
                }
            }
            return tempList;
        }

        public static Robot.Common.Robot FindEnemy(string ownerName, Robot.Common.Robot robot,
            IList<Robot.Common.Robot> allRobots)
        {
            var possibleEnemies = allRobots.Where(x => x.OwnerName != ownerName).ToList();
            var biggestProfit = (double)Constants.ROBOTENERGYCOLLECTION;
            var bestEnemy = new Robot.Common.Robot();
            var bestEnemyFound = false;
            foreach (var possibleEnemy in possibleEnemies)
            {
                if (CalculateEnergyOnRoad(possibleEnemy.Position, robot.Position) + Constants.ENERGYTAKENTODAMAGE <
                    possibleEnemy.Energy * Constants.PERCENTSRECIEVEFORDAMAGE)
                {
                    var profit = possibleEnemy.Energy * Constants.PERCENTSRECIEVEFORDAMAGE -
                                 CalculateEnergyOnRoad(possibleEnemy.Position, robot.Position) +
                                 Constants.ENERGYTAKENTODAMAGE;
                    if (profit > biggestProfit)
                    {
                        bestEnemyFound = true;
                        biggestProfit = profit;
                        bestEnemy = possibleEnemy;
                    }
                }
            }
            if (bestEnemyFound)
                return bestEnemy;
            return null;
        }
    }
}
