﻿namespace NykolaichukMaksym.RobotChallenge.Utils
{
    public static class Constants
    {
        public const int MINDISTANCETOCOLLECTENERGY = 2;
        public const int ROBOTENERGYCOLLECTION = 40;
        public const double PERCENTSRECIEVEFORDAMAGE = 0.1;
        public const int ENERGYTAKENTODAMAGE = 20;
        public const int REPRODUCINGENERGYCOUNTRULE = 200;
        public const int INITIALROBOTSCOUNT = 10;
        public const int MAXROBOTSCOUNT = 100;
        public const int LOCKEDSTATIONRULE = 1;
    }
}
