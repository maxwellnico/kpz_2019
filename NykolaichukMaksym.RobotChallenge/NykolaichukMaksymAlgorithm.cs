﻿using NykolaichukMaksym.RobotChallenge.Utils;
using Robot.Common;
using System.Collections.Generic;
using System.Linq;

namespace NykolaichukMaksym.RobotChallenge
{
    public class NykolaichukMaksymAlgorithm : IRobotAlgorithm
    {
        public string Author => "Maksym Nykolaichuk";

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var currentRobot = robots[robotToMoveIndex];
            var isRobotStandsOnStation = map.Stations.Count(x => x.Position == currentRobot.Position) == 1;
            var nearestStation = RobotUtils.FindNearestNotTakenStation(Author, currentRobot, robots, map.Stations);

            if (isRobotStandsOnStation)
            {
                if (currentRobot.Energy >= Constants.REPRODUCINGENERGYCOUNTRULE &&
                    robots.Count(x => x.OwnerName == Author) != Constants.MAXROBOTSCOUNT)
                    return new CreateNewRobotCommand();
                return new CollectEnergyCommand();
            }
            if (RobotUtils.CanMoveInOneStep(currentRobot, nearestStation.Position)
                && RobotUtils.CalculateDistance(nearestStation.Position, currentRobot.Position) > Constants.MINDISTANCETOCOLLECTENERGY)
            {
                var enemy = RobotUtils.FindEnemy(Author, currentRobot, robots);
                if (enemy == null)
                    return new MoveCommand()
                    {
                        NewPosition = nearestStation.Position
                    };
                else
                {
                    return new MoveCommand()
                    {
                        NewPosition = enemy.Position
                    };
                }

            }

            return new MoveCommand()
            {
                NewPosition = RobotUtils.CertainStepsTowardsPosition(currentRobot.Position, nearestStation.Position, 2)
            };

        }
    }
}
