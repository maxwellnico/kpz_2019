﻿using System.Collections;
using System.Collections.Generic;
using Robot.Common;

namespace RobotChallenge.UnitTests.Data
{
    public static class DefaultDataPresets
    {
        public static List<EnergyStation> EnergyStations = new List<EnergyStation>()
        {
            new EnergyStation()
            {
                Energy = 110,
                Position = new Position(0,0),
                RecoveryRate = 10,
            },
            new EnergyStation()
            {
                Energy = 35,
                Position = new Position(10,10),
                RecoveryRate = 20,
            },
            new EnergyStation()
            {
                Energy = 30,
                Position = new Position(20,20),
                RecoveryRate = 30,
            },
            new EnergyStation()
            {
                Energy = 40,
                Position = new Position(30,30),
                RecoveryRate = 40,
            },
            new EnergyStation()
            {
                Energy = 40,
                Position = new Position(40,50),
                RecoveryRate = 40,
            },
            new EnergyStation()
            {
                Energy = 50,
                Position = new Position(50,60),
                RecoveryRate = 50,
            },
        };
    }
}