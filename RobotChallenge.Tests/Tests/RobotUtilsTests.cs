using FluentAssertions;
using NykolaichukMaksym.RobotChallenge.Utils;
using Robot.Common;
using RobotChallenge.UnitTests.Data;
using System;
using Xunit;

namespace RobotChallenge.UnitTests.Tests
{
    public class RobotUtilsTests
    {
        private const double defaultDeviation = 0.0001;

        [Theory]
        [InlineData(0, 0, 1, 1)]
        [InlineData(1, 1, 1, 1)]
        [InlineData(0, 0, 0, 0)]
        [InlineData(1, 4, 4, 1)]
        [InlineData(999, 999, -999, -999)]
        public void CalculateDistanceBetweenPoints_NormalState_ExpectCorrectCalculation(int x1, int y1, int x2, int y2)
        {
            var positionOne = new Position(x1, y1);
            var positionTwo = new Position(x2, y2);

            var distance = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));

            RobotUtils.CalculateDistance(positionOne, positionTwo).Should()
                .BeInRange(distance - defaultDeviation, distance + defaultDeviation, "Impossible to compare doubles");
        }
        [Theory]
        [InlineData(0, 0, 1, 1)]
        [InlineData(1, 1, 1, 1)]
        [InlineData(0, 0, 0, 0)]
        [InlineData(1, 4, 4, 1)]
        [InlineData(999, 999, -999, -999)]
        public void CalculateEnergySpentPoints_NormalState_ExpectedFineCalculatedSpentEnergy(int x1, int y1, int x2, int y2)
        {
            var positionOne = new Position(x1, y1);
            var positionTwo = new Position(x2, y2);

            var energyTaken = (Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));

            RobotUtils.CalculateEnergyOnRoad(positionOne, positionTwo).Should()
                .BeInRange(energyTaken - defaultDeviation, energyTaken + defaultDeviation, "Impossible to compare doubles");
        }

        [Fact]
        public void SortByEnergy_NormalState_ExpectedSortedListOfEnergyStations()
        {
            var energyStationsList = DefaultDataPresets.EnergyStations;

            var sortedEnergyStationsList = RobotUtils.SortByEnergy(energyStationsList);

            sortedEnergyStationsList.Count.Should().Be(energyStationsList.Count);
            for (int i = 1; i < sortedEnergyStationsList.Count; i++)
            {
                sortedEnergyStationsList[i].Energy.Should().BeGreaterOrEqualTo(sortedEnergyStationsList[i - 1].Energy);
            }
        }

        [Fact]
        public void SortByDistance_NormalState_ExpectedSortedListOfEnergyStations()
        {
            var energyStationsList = DefaultDataPresets.EnergyStations;
            var robot = new Robot.Common.Robot()
            {
                Position = new Position(0, 0)
            };

            var sortedEnergyStationsList = RobotUtils.SortByDistance(robot, energyStationsList);

            sortedEnergyStationsList.Count.Should().Be(energyStationsList.Count);
            for (int i = 1; i < sortedEnergyStationsList.Count; i++)
            {
                RobotUtils.CalculateDistance(robot.Position, sortedEnergyStationsList[i].Position).Should()
                    .BeGreaterOrEqualTo(RobotUtils.CalculateDistance(robot.Position,
                        sortedEnergyStationsList[i - 1].Position));
            }
        }

        [Theory]
        [InlineData(0, 0, 0, 1, 1)]
        [InlineData(10, 1, 1, 1, 1)]
        [InlineData(100, 0, 0, 0, 0)]
        [InlineData(6, 1, 4, 4, 1)]
        [InlineData(999, 999, 999, -999, -999)]
        public void CanMoveInOneStep_NormalState_ExpectToDoOneStepTowardsPosition(int initialEnergy, int x1, int y1, int x2, int y2)
        {
            var positionOne = new Position(x1, y1);
            var positionTwo = new Position(x2, y2);

            var robot = new Robot.Common.Robot()
            {
                Position = positionOne,
                Energy = initialEnergy
            };

            var energyTaken = (Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));

            RobotUtils.CanMoveInOneStep(robot, positionTwo).Should().Be(initialEnergy > energyTaken);
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(25, 25)]
        [InlineData(10, 10)]
        [InlineData(5, 5)]
        [InlineData(999, 999)]
        public void FindNearestStation_NormalState_ExpectFindStationWithLowestDistanceTo(int x1, int y1)
        {

            var positionOne = new Position(x1, y1);
            var robot = new Robot.Common.Robot()
            {
                Position = positionOne
            };
            var energyStations = DefaultDataPresets.EnergyStations;

            var closestEnergyStation = energyStations[0];

            foreach (var energyStation in energyStations)
            {
                if (RobotUtils.CalculateDistance(energyStation.Position, robot.Position) <
                    RobotUtils.CalculateDistance(closestEnergyStation.Position, robot.Position))
                    closestEnergyStation = energyStation;
            }

            closestEnergyStation.Should().Be(RobotUtils.FindNearestStation(robot, energyStations));
        }

        [Theory]
        [InlineData(9999, 0, 0, 1, 1)]
        [InlineData(9999, 1, 20, -1, 1)]
        [InlineData(9999, 0, 0, 0, 0)]
        [InlineData(9999, -1, -4, 4, 1)]
        public void CanMoveInOneStep_NormalState_ExpectToBeStillInPosition(int stepsTowards, int x1, int y1, int x2, int y2)
        {
            var positionOne = new Position(x1, y1);
            var positionTwo = new Position(x2, y2);

            var finishingPosition = RobotUtils.CertainStepsTowardsPosition(positionOne, positionTwo, stepsTowards);

            finishingPosition.Should().Be(positionTwo);
        }
    }
}
